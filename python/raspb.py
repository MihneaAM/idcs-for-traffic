import cv2
from skimage import measure, io, filters
import numpy as np	
import imutils
import os

refPt = [(25, 358), (300, 393),
		(25, 316), (300, 351),
		(318, 17), (351, 215),
		(359, 17), (392, 215),
		(495, 233), (771, 266),
		(495, 273), (771, 306),
		(443, 410), (477, 597),
		(401, 410), (436, 597)]

rois = []
vector = []

v_capture = cv2.VideoCapture(1)
v_capture.set(3, 800)
v_capture.set(4, 600)

bg = cv2.imread('bgbg.jpg')
fgbg = cv2.bgsegm.createBackgroundSubtractorMOG()
fgbg.apply(bg, learningRate=0.5)

while(1):
	ret, frame = v_capture.read()

	for i in xrange(0, len(refPt), 2):
		cv2.rectangle(frame, refPt[i], refPt[i+1], (0, 0, 255), 1)
	cv2.imshow('frame', frame)

	fgmask = fgbg.apply(frame, learningRate=0)

	cv2.imshow('fgmask', fgmask)

	kernel = np.ones((3, 3), np.uint8)
	fgmask = cv2.erode(fgmask, None, iterations=1)
	fgmask = cv2.dilate(fgmask, None, iterations=9)
	fgmask = cv2.erode(fgmask, None, iterations=1)

	cv2.imshow('fgmask_after', fgmask)

	left_1 = fgmask[refPt[0][1]:refPt[1][1]+1, refPt[0][0]:refPt[1][0]+1]
	left_2 = fgmask[refPt[2][1]:refPt[3][1]+1, refPt[2][0]:refPt[3][0]+1]

	top_1 = fgmask[refPt[4][1]:refPt[5][1]+1, refPt[4][0]:refPt[5][0]+1]
	top_2 = fgmask[refPt[6][1]:refPt[7][1]+1, refPt[6][0]:refPt[7][0]+1]

	right_1 = fgmask[refPt[8][1]:refPt[9][1]+1, refPt[8][0]:refPt[9][0]+1]
	right_2 = fgmask[refPt[10][1]:refPt[11][1]+1, refPt[10][0]:refPt[11][0]+1]

	bottom_1 = fgmask[refPt[12][1]:refPt[13][1]+1, refPt[12][0]:refPt[13][0]+1]
	bottom_2 = fgmask[refPt[14][1]:refPt[15][1]+1, refPt[14][0]:refPt[15][0]+1]

	labels_left_1 = measure.label(left_1)
	labels_left_2 = measure.label(left_2)

	labels_top_1 = measure.label(top_1)
	labels_top_2 = measure.label(top_2)

	labels_right_1 = measure.label(right_1)
	labels_right_2 = measure.label(right_2)

	labels_bottom_1 = measure.label(bottom_1)
	labels_bottom_2 = measure.label(bottom_2)

	
	vector.append(labels_left_1.max())
	vector.append(labels_left_2.max())
	vector.append(labels_top_1.max())
	vector.append(labels_top_2.max())
	vector.append(labels_right_1.max())
	vector.append(labels_right_2.max())
	vector.append(labels_bottom_1.max())
	vector.append(labels_bottom_2.max())

	output = sorted(range(len(vector)), key = lambda k : vector[k])
	output = output[::-1]
	output = [x+1 for x in output]

	#print(output)

	os.system("~/433Utils/RPi_utils/codesend %d%d%d%d%d%d%d%d" % (output[0], output[1], output[2], output[3], output[4], output[5], output[6], output[7]))

	k = cv2.waitKey(1) & 0xFF
	if k == 27:
		break


cv2.release()
cv2.destroyAllWindows()