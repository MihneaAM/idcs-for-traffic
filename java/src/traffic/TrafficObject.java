package traffic;

import javafx.geometry.Point2D;
import javafx.scene.Node;

/**
 *
 * @author mihneaam
 */

public class TrafficObject {
    private Node view;
    
    private boolean alive = true;
    
    public TrafficObject(Node view) {
        this.view = view;
    }
    
    public Node getView(){
        return view;
    }
    
    /*public boolean isAlive(){
        return alive;
    }*/
    
    public boolean isDead(){
        return !alive;
    }
    
    public void setAlive(boolean alive){
        this.alive = alive;
    }    
}
