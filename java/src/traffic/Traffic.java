/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package traffic;



import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javafx.animation.AnimationTimer;
import javafx.animation.PathTransition;
import javafx.animation.PathTransition.OrientationType;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.StrokeLineCap;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author mihneaam
 */
public class Traffic extends Application {
  
    Pane root;
    Random random = new Random(System.currentTimeMillis());
    
    ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
    
    TranslateTransition transition = new TranslateTransition(Duration.seconds(5));
    
    private List<TrafficObject> cars_left_1 = new ArrayList<>();
    private List<TrafficObject> cars_left_2 = new ArrayList<>();
    
    private List<TrafficObject> cars_right_1 = new ArrayList<>();
    private List<TrafficObject> cars_right_2 = new ArrayList<>();
    
    private List<TrafficObject> cars_top_1 = new ArrayList<>();
    private List<TrafficObject> cars_top_2 = new ArrayList<>();
    
    private List<TrafficObject> cars_bottom_1 = new ArrayList<>();
    private List<TrafficObject> cars_bottom_2 = new ArrayList<>();
    
    private List<ComboBox<Integer>> levels = new ArrayList<>(8);
    
    private List<Circle> lights = new ArrayList<>();
    
    List<Integer> list = new ArrayList<Integer>();
    File file = new File("file.txt");
    BufferedReader reader = null;
    
    int count = 0;
    
    Button btn = new Button();
            
    private void addStraightTransition(TrafficObject car, TranslateTransition sTransition, int dist, int angle) {
        
        sTransition.setNode(car.getView());
        
        switch(angle) {
            case 0:
                sTransition.setToX(dist);
                break;
            
            case 90:
                sTransition.setToY(dist);
                break;
                
            case 180:
                sTransition.setToX(dist);
                break;
                
            case 270:
                sTransition.setToY(dist);
                break;
        }  
        
        sTransition.play();   
    }
                         
    private void addStraightPath(TrafficObject car, int endX, int endY, int seconds, String direction) {
        
        MoveTo start = new MoveTo();
        start.setX(car.getView().getTranslateX()+38);
        start.setY(car.getView().getTranslateY()+38);
        
        LineTo linePath = new LineTo();
        
        switch(direction){
            case "VE":
                linePath.setX(endX);
                linePath.setY(car.getView().getTranslateY()+38);
                break;
                
            case "NS":
                linePath.setX(car.getView().getTranslateX()+38);
                linePath.setY(endY);
                break;
  
        }
        
        Path path = new Path();
        path.setStroke(Color.BLUE);
        path.setStrokeWidth(3);
        path.getElements().add(start);
        path.getElements().add(linePath);
        
        PathTransition pathTransition = new PathTransition();
        pathTransition.setDuration(Duration.seconds(seconds+4));
        pathTransition.setNode(car.getView());
        pathTransition.setPath(path);
        pathTransition.setOrientation(OrientationType.ORTHOGONAL_TO_TANGENT);
        
        pathTransition.play();
        
        root.getChildren().addAll(path);
    }
    
    private void addCurvePath(TrafficObject car, int endX, int endY, int cX1, int cY1, int cX2, int cY2, int seconds, String direction) {
        
        MoveTo start = new MoveTo();
        start.setX(car.getView().getTranslateX()+38); // 425
        start.setY(car.getView().getTranslateY()+38); // 605
        
        LineTo linePath = new LineTo();
        
        switch(direction){
            case "VE":
                linePath.setX(463);
                linePath.setY(car.getView().getTranslateY()+38);
                break;
                
            case "NS":
                linePath.setX(car.getView().getTranslateX()+38);
                linePath.setY(323);
                break;
                
            case "EV":
                linePath.setX(816);
                linePath.setY(car.getView().getTranslateY()+38);
                break;
                
            case "SN":
                linePath.setX(car.getView().getTranslateX()+38);
                linePath.setY(676);
                break;    
        }
        
        
        CubicCurveTo curvePath = new CubicCurveTo();
        curvePath.setX(endX);
        curvePath.setY(endY);
        curvePath.setControlX1(cX1);
        curvePath.setControlY1(cY1);
        curvePath.setControlX2(cX2);
        curvePath.setControlY2(cY2);
        
        Path path2 = new Path();
        path2.setStroke(Color.RED);
        path2.setStrokeWidth(3);
        path2.getElements().add(start);
        path2.getElements().add(linePath);
        path2.getElements().add(curvePath);
        
        PathTransition pathTransition2 = new PathTransition();
        pathTransition2.setDuration(Duration.seconds(seconds+4));
        pathTransition2.setNode(car.getView());
        pathTransition2.setPath(path2);
        pathTransition2.setOrientation(OrientationType.ORTHOGONAL_TO_TANGENT);
        
        pathTransition2.play();
        
        root.getChildren().addAll(path2);
    }

    private void addCar(List<TrafficObject> vector, TrafficObject car, double posX, double posY, int angle, int dist) {
        
        vector.add(car);
        car.getView().setTranslateX(posX);
        car.getView().setTranslateY(posY);
        car.getView().setRotate(angle);
        
        root.getChildren().add(car.getView());
        
        /*trans.setNode(car.getView());
        trans.setToX(xs);
        trans.setDelay(Duration.seconds(xss));
        trans.play();*/
        
        addStraightTransition(car, new TranslateTransition(Duration.seconds(3)), dist, angle);
    }
    
    
    
    int[] position = new int[5];
    int[] xss = new int[4];

    
    int index;
    int value_long, value_short;
    boolean xy = true;
    boolean fis = true;
    
    private void onUpdate() {
        
        boolean tmm = false;
        for(TrafficObject car : cars_left_1) { // verific daca masina a plecat de la semafor
            if(car.getView().getTranslateX() > 350) {
                car.setAlive(false);
                root.getChildren().remove(car.getView());
                tmm = true;
            } 
        }

        cars_left_1.removeIf(TrafficObject::isDead);
        cars_left_2.removeIf(TrafficObject::isDead);
        
        cars_top_1.removeIf(TrafficObject::isDead);
        cars_top_2.removeIf(TrafficObject::isDead);
        
        cars_right_1.removeIf(TrafficObject::isDead);
        cars_right_2.removeIf(TrafficObject::isDead);
        
        cars_bottom_1.removeIf(TrafficObject::isDead);
        cars_bottom_2.removeIf(TrafficObject::isDead);
        
        index = 0;
       
        /*if(fis == true) {
            
            try {
            
                reader = new BufferedReader(new FileReader(file));
                String text = null;

                while ((text = reader.readLine()) != null) {
                    list.add(Integer.parseInt(text));
                }
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
            } 
            catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                try {
                    if (reader != null) {
                        
                        reader.close();
                    }
                }
                catch (IOException e) {
                }
            }
            fis = false;
            //timer.schedule( new Transitions() , 0, 5000 );
        } */
    }
    
    private void populate() {
        
        if(cars_left_1.isEmpty() && cars_left_2.isEmpty() && cars_top_1.isEmpty() && cars_top_2.isEmpty()
                && cars_right_1.isEmpty() && cars_right_2.isEmpty() && cars_bottom_1.isEmpty() && cars_bottom_2.isEmpty()) {
            for(ComboBox<Integer> level : levels) {  
                
                if(level.getValue() == 0) {
                    value_long = random.nextInt(6);
                    value_short = random.nextInt(5);
                }
                else {
                    value_long = level.getValue();
                    value_short = level.getValue();
                }
                
                switch(levels.indexOf(level)) {
                    case 0:
                        while(cars_left_1.size() < value_long) {
                            addCar(cars_left_1, new Car(), -150, 565, 0, position[cars_left_1.size()]);
                        }
                        break;

                    case 1:
                        while(cars_left_2.size() < value_long) { 
                            addCar(cars_left_2, new Car(), -150, 495, 0, position[cars_left_2.size()]);
                        }
                        break;
                        
                    case 2:
                        while(cars_top_1.size() < value_short) {
                            addCar(cars_top_1, new Car(), 495, -150, 90, position[cars_top_1.size()]-140);
                        }
                        break;

                    case 3:
                        while(cars_top_2.size() < value_short) {
                            addCar(cars_top_2, new Car(), 565, -150, 90, position[cars_top_2.size()]-140);
                        }
                        break;

                    case 4:
                        while(cars_right_1.size() < value_long) {
                            addCar(cars_right_1, new Car(), 1429, 355, 180, 1279-(position[cars_right_1.size()]+85));
                        }
                        break;

                    case 5:
                        while(cars_right_2.size() < value_long) {
                            addCar(cars_right_2, new Car(), 1429, 425, 180, 1279-(position[cars_right_2.size()]+85));
                        }
                        break;

                    case 6:
                        while(cars_bottom_1.size() < value_short) {
                            addCar(cars_bottom_1, new Car(), 705, 1150, 270, 999-(position[cars_bottom_1.size()]-65));
                        }
                        break;

                    case 7:
                        while(cars_bottom_2.size() < value_short) {
                            addCar(cars_bottom_2, new Car(), 635, 1150, 270, 999-(position[cars_bottom_2.size()]-65));
                        }
                        break;
                }
            }
            //executor.scheduleAtFixedRate(helloRunnable, 5, 7, TimeUnit.SECONDS);
        }
        
    }
    
    public abstract class FileWatcher extends TimerTask {
        
        private long timeStamp;
        private final File file;

        public FileWatcher( File file ) {
            
            this.file = file;
            this.timeStamp = file.lastModified();
        }

        public final void run() {
    
            long timeStamp = file.lastModified();

            if( this.timeStamp != timeStamp ) {
      
                this.timeStamp = timeStamp;
                onChange(file);
            }
        }

        protected abstract void onChange( File file );
    }
    
    private static class Car extends TrafficObject {
        Car() {
            super(new ImageView("file:car.png")); // prin constructorul TrafficObjects
        }
    }
    
    private void addGrass(Region rec, int sizeX, int sizeY, int posX, int posY, String card) {
        
        rec.setPrefSize(sizeX, sizeY);
        rec.setLayoutX(posX);
        rec.setLayoutY(posY);
        
        switch(card) {
            case "NV":
                rec.setStyle("-fx-background-color: forestgreen; -fx-background-radius: 0 0 50 0");
                break;
              
            case "SV":
                rec.setStyle("-fx-background-color: forestgreen; -fx-background-radius: 0 50 0 0");
                break;
                
            case "NE":
                rec.setStyle("-fx-background-color: forestgreen; -fx-background-radius: 0 0 0 50");
                break;
                
            case "SE":
                rec.setStyle("-fx-background-color: forestgreen; -fx-background-radius: 50 0 0 0");
                break;
        }
        root.getChildren().add(rec);
    }
    
    private void addRoad(Region rec, int sizeX, int sizeY, int posX, int posY) {
        
        rec.setPrefSize(sizeX, sizeY);
        rec.setLayoutX(posX);
        rec.setLayoutY(posY);
        rec.setStyle("-fx-background-color: grey");
        
        root.getChildren().add(rec);
    }
    
    private void addDashLine(Line dLine, double startX, double startY, double endX, double endY) {
       
        dLine.setStartX(startX);
        dLine.setStartY(startY);
        dLine.setEndX(endX);
        dLine.setEndY(endY);
        
        dLine.setStroke(Color.WHITE);
        dLine.setStrokeWidth(2);
        dLine.setStrokeLineCap(StrokeLineCap.BUTT);
        dLine.getStrokeDashArray().addAll(10d, 30d);
        dLine.setStrokeDashOffset(0);
        
        root.getChildren().add(dLine);
    }
    
    private void addStraightLine(Line sLine, double startX, double startY, double endX, double endY, Color color, StrokeLineCap slc, int width) {
        
        sLine.setStartX(startX);
        sLine.setStartY(startY);
        sLine.setEndX(endX);
        sLine.setEndY(endY);
        
        sLine.setStroke(color);
        sLine.setStrokeWidth(width);
        sLine.setStrokeLineCap(slc);
        
        root.getChildren().add(sLine);
    }
    
    private void addLight(Circle circle, double posX, double posY, double radius) {
        
        lights.add(circle);
        circle.setCenterX(posX);
        circle.setCenterY(posY);
        circle.setRadius(radius);
        circle.setFill(Color.LIGHTCYAN);
        
        root.getChildren().add(circle);
    }
    
    int ii = -1;
    private void addLocation(final Label label) {
        
        root.setStyle("-fx-font-size: 15px");
        
        root.getChildren().add(label);
        
        
        
        root.setOnMouseMoved(new EventHandler<MouseEvent> () {
            @Override
            public void handle(MouseEvent event) {
                String msg =
              "(x: "       + event.getX()      + ", y: "       + event.getY()       + ") -- " +
              "(sceneX: "  + event.getSceneX() + ", sceneY: "  + event.getSceneY()  + ") -- " +
              "(screenX: " + event.getScreenX()+ ", screenY: " + event.getScreenY() + ")";

                label.setText(msg);
            }
        });
        
        root.setOnMouseExited(new EventHandler<MouseEvent>() {
        
            @Override
            public void handle(MouseEvent event) {
                label.setText("Outside pane");
            }
        });
    }
    
    private void addComboBox(ComboBox<Integer> levelCB, int posX, int posY) {
        
        levels.add(levelCB);
        levelCB.setValue(0);
        if(levels.size() == 1 || levels.size() == 2 || levels.size() == 5 || levels.size() == 6) {
            levelCB.getItems().addAll(0, 1, 2, 3, 4, 5);
        } else {
            levelCB.getItems().addAll(0, 1, 2, 3, 4);
        }
        
        levelCB.setLayoutX(posX);
        levelCB.setLayoutY(posY);
        
        root.getChildren().add(levelCB);  
    }
    
    
    
    private static Robot robot = null;
    static int counter = 0;
    
    public static void klick() {
        
        if(counter < 8) {
            //robot.keyPress(y);
            //robot.mouseMove(x, y);
            robot.delay(5);
            //robot.mousePress(InputEvent.BUTTON1_MASK);
            //robot.mouseRelease(InputEvent.BUTTON1_MASK);
            robot.keyPress('Z');
            robot.keyRelease('Z');
            counter++;
        }
    }
    
    Runnable helloRunnable = () -> {
        klick();
    };
    
    private Parent createMap() {
       
        root = new Pane();
        root.setPrefSize(1279, 999);
        
        addRoad(new Region(), 1279, 999, 0, 0); //1000, 700
        
        addGrass(new Region(), 500, 360, 0, 0, "NV"); // 400, 250, 0, 0
        addGrass(new Region(), 500, 360, 0, 639, "SV"); // 400, 250, 0, 450
        addGrass(new Region(), 500, 360, 779, 0, "NE"); // 400, 250, 600, 0
        addGrass(new Region(), 500, 360, 779, 639, "SE"); // 400, 250, 600, 450
        
        addDashLine(new Line(), 0, 428, 470, 428); // left_top_h // 0, 300, 400, 300
        addDashLine(new Line(), 0, 570, 470, 570); // left_bottom_h // 0, 400, 250, 400
        addDashLine(new Line(), 1029, 428, 1279, 428); // right_top_h // 750, 300, 1000, 300
        addDashLine(new Line(), 809, 570, 1279, 570); // right_bottom_h // 630, 400, 1000, 400
        addDashLine(new Line(), 568, 0, 568, 110); // top_left_v // 450, 0, 450, 100
        addDashLine(new Line(), 710, 0, 710, 330); // top_right_v // 550, 0, 550, 230
        addDashLine(new Line(), 568, 669, 568, 999); // bottom_left_v // 450, 500, 450, 700
        addDashLine(new Line(), 710, 710, 710, 999); // bottom_right_v // 550, 600, 550, 700
        
        addStraightLine(new Line(), 250, 570, 470, 570, Color.WHITE, StrokeLineCap.BUTT, 2); // left_bottom_h // 250, 400, 370, 400
        addStraightLine(new Line(), 809, 428, 1029, 428, Color.WHITE, StrokeLineCap.BUTT, 2); // right_top_h // 630, 300, 750, 300
        addStraightLine(new Line(), 568, 110, 568, 330, Color.WHITE, StrokeLineCap.BUTT, 2); // top_left_v // 450, 120, 450, 210
        addStraightLine(new Line(), 710, 669, 710, 889, Color.WHITE, StrokeLineCap.BUTT, 2); // bottom_right_v // 550, 500, 550, 600
        
        addStraightLine(new Line(), 0, 497, 470, 497, Color.GAINSBORO, StrokeLineCap.ROUND, 5); // left_h // 0, 350, 370, 350
        addStraightLine(new Line(), 809, 497, 1279, 497, Color.GAINSBORO, StrokeLineCap.ROUND, 5); // right_h //630, 350, 1000, 350
        addStraightLine(new Line(), 637, 0, 637, 330, Color.GAINSBORO, StrokeLineCap.ROUND, 5); // top_v // 500, 0, 500, 210
        addStraightLine(new Line(), 637, 669, 637, 999, Color.GAINSBORO, StrokeLineCap.ROUND, 5); // bottom_v // 500, 500, 500, 700
        
        addLight(new Circle(), 500, 607, 10);
        addLight(new Circle(), 500, 531, 10);
        addLight(new Circle(), 533, 360, 10);
        addLight(new Circle(), 603, 360, 10);
        addLight(new Circle(), 779, 393, 10);
        addLight(new Circle(), 779, 463, 10);
        addLight(new Circle(), 743, 639, 10);
        addLight(new Circle(), 673, 639, 10);
        
        addLocation(new Label("Outside Pane"));
            
        addComboBox(new ComboBox<Integer>(), 10, 690); // 10, 500
        addComboBox(new ComboBox<Integer>(), 10, 650); // 10, 460
        addComboBox(new ComboBox<Integer>(), 300, 20); // 300, 100
        addComboBox(new ComboBox<Integer>(), 370, 20); //630, 100
        addComboBox(new ComboBox<Integer>(), 1200, 280); // 900, 150
        addComboBox(new ComboBox<Integer>(), 1200, 320); // 900, 190
        addComboBox(new ComboBox<Integer>(), 859, 950); // 630, 550
        addComboBox(new ComboBox<Integer>(), 789, 950); // 300, 550
        
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                onUpdate();
            }
        };
        timer.start();

        return root;
    }
    
        
    @Override
    public void start(Stage primaryStage)
    {
        
        
        /*TimerTask task = new FileWatcher( new File("file.txt") ) {
            protected void onChange( File file ) {
                // here we code the action on a change
                System.out.println( "File "+ file.getName() +" have change !" );
            }
        };*/
        
       try {
           robot = new Robot();
       } catch(AWTException x) {
           x.printStackTrace();
       }
       

        btn.setLayoutY(100);
        btn.setOnAction((ActionEvent e) -> {
            populate();
        });
        
        
        
        
        
        Scene scene = new Scene(createMap()); //Grey

        root.getChildren().add(btn);
        
        
        primaryStage.setTitle("Traffic");
        primaryStage.setScene(scene);
        primaryStage.show();

        position[0] = 380;
        position[1] = 280;
        position[2] = 180;
        position[3] = 80;
        position[4] = -20;
        
        xss[0] = 0;
        xss[1] = 1;
        xss[2] = 2;
        xss[3] = 3;
        
        root.setOnKeyPressed((KeyEvent x) -> {
            if((x.getCode() == KeyCode.Z) && (ii < 8)) {
                ii++;
                switch (list.get(ii)) {
                    case 0:
                        for (int index1 = 0; index1 < cars_left_1.size(); index1++) {
                            if (random.nextInt(2) == 0) {
                                addStraightPath(cars_left_1.get(index1), 1200, 0, index1 * 3, "VE");
                            } else {
                                addCurvePath(cars_left_1.get(index1), 535, 990, 535, 605, 535, 605, index1 * 2, "VE"); // 535, 605
                            }
                        }
                        break;
                    case 1:
                        for (int index2 = 0; index2 < cars_left_2.size(); index2++) {
                            if (random.nextInt(2) == 0) {
                                addStraightPath(cars_left_2.get(index2), 1200, 0, index2 * 3, "VE");
                            } else {
                                addCurvePath(cars_left_2.get(index2), 673, 50, 673, 535, 673, 535, index2 * 2, "VE"); // 535, 605
                            }
                        }
                        break;
                    case 2:
                        for (int index3 = 0; index3 < cars_top_1.size(); index3++) {
                            if (random.nextInt(2) == 0) {
                                addStraightPath(cars_top_1.get(index3), 0, 990, index3 * 3, "NS");
                            } else {
                                addCurvePath(cars_top_1.get(index3), 50, 393, 535, 393, 535, 393, index3 * 2, "NS"); // 535, 605
                            }
                        }
                        break;
                    case 3:
                        for (int index4 = 0; index4 < cars_top_2.size(); index4++) {
                            if (random.nextInt(2) == 0) {
                                addStraightPath(cars_top_2.get(index4), 0, 990, index4 * 3, "NS");
                            } else {
                                addCurvePath(cars_top_2.get(index4), 1200, 603, 603, 603, 603, 603, index4 * 2, "NS"); // 535, 605
                            }
                        }
                        break;
                    case 4:
                        for (int index5 = 0; index5 < cars_right_1.size(); index5++) {
                            if (random.nextInt(2) == 0) {
                                addStraightPath(cars_right_1.get(index5), 50, 0, index5 * 3, "VE");
                            } else {
                                addCurvePath(cars_right_1.get(index5), 743, 50, 743, 393, 743, 393, index5 * 2, "EV"); // 535, 605
                            }
                        }
                        break;
                    case 5:
                        for (int index6 = 0; index6 < cars_right_2.size(); index6++) {
                            if (random.nextInt(2) == 0) {
                                addStraightPath(cars_right_2.get(index6), 50, 0, index6 * 3, "VE");
                            } else {
                                addCurvePath(cars_right_2.get(index6), 533, 990, 533, 463, 533, 463, index6 * 2, "EV"); // 535, 605
                            }
                        }
                        break;
                    case 6:
                        for (int index7 = 0; index7 < cars_bottom_1.size(); index7++) {
                            if (random.nextInt(2) == 0) {
                                addStraightPath(cars_bottom_1.get(index7), 50, 0, index7 * 3, "NS");
                            } else {
                                addCurvePath(cars_bottom_1.get(index7), 1200, 603, 743, 603, 743, 603, index7 * 2, "SN"); // 535, 605
                            }
                        }
                        break;
                    case 7:
                        for (int index8 = 0; index8 < cars_bottom_2.size(); index8++) {
                            if (random.nextInt(2) == 0) {
                                addStraightPath(cars_bottom_2.get(index8), 50, 0, index8 * 3, "NS");
                            } else {
                                lights.get(7).setFill(Color.GREEN);
                                addCurvePath(cars_bottom_2.get(index8), 50, 463, 673, 463, 673, 463, index8 * 2, "SN"); // 535, 605
                            }
                        }
                        break;
                }
            }
        });
        
        try {
            
            reader = new BufferedReader(new FileReader(file));
            String text = null;

            while ((text = reader.readLine()) != null) {
            
                    list.add(Integer.parseInt(text));
            }
        }
        
        catch (FileNotFoundException e) {
        
            e.printStackTrace();
        } 
        
        catch (IOException e) {
            
            e.printStackTrace();
        }
        finally {
            try {
                if (reader != null) {
                        
                    reader.close();
                }
            }
            catch (IOException e) {
                
                e.printStackTrace();
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
